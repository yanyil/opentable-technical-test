
# Opentable technical exercise

This project is for the technical exercise of Opentable.

## Available Scripts

In the project directory, you can run:

### `yarn`

Install the dependencies.

### `yarn start`

Runs the app in the development mode.   
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.  

### `yarn test`

Launches the test runner and run the tests once.  


### `yarn build`

Builds the app for production to the `dist` folder.  
It correctly bundles React in production mode and optimizes the build for the best performance.  

The build is minified and the filenames include the hashes.  


