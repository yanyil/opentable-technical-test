const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const autoprefixer = require('autoprefixer');

const BUILD_DIR = path.resolve(__dirname, 'dist/');
const SOURCE_DIR = path.resolve(__dirname, 'src/');

module.exports = {
  entry: `${SOURCE_DIR}/index.jsx`,
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['.js', '.json', '.jsx', '.ts', '.tsx', '.node'],
    alias: {
      components: path.resolve(`${SOURCE_DIR}/components/`),
      services: path.resolve(__dirname, 'src/services/')
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|json)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
        }
      }, {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: [
                autoprefixer,
              ],
            },
          },
        ],
      }, {
        test: /\.(png|jpg|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
          },
        }],
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `${SOURCE_DIR}/index.html`,
    }),
    new CleanWebpackPlugin(),
  ],
};
