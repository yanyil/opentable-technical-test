import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from '.'

Enzyme.configure({ adapter: new Adapter() });

describe('<App />', () => {
  let wrapper;
  
  beforeEach(() => {
    wrapper = Enzyme.shallow(<App />);
  });

  it('renders without crashing', () => {
    expect(wrapper.exists()).toBe(true);
  });
})

