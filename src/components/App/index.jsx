import React from 'react';
import GuestsList from 'components/guests-list';
import {getGuestsData} from 'services/mock.service';


export default class App extends React.PureComponent {
  render() {
  return <React.Fragment>
    <h1 className={`text-2xl`}>Guests</h1>
    <GuestsList guests={getGuestsData().data}></GuestsList>
  </React.Fragment>
  }
};
