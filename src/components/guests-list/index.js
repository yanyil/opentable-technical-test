import React from 'react';
import GuestItem from 'components/guest-item';

export default class GuestsList extends React.PureComponent {
    constructor(props) {
        super(props);
        
        this.state = {
            guests: props.guests,
            showMarketing: false,
            sortBy: "id"
        };
    }

    render() {
        return <div className={`p-2`}>
            <div className={`text-right my-4`}>
                <button type="button" className={`btn btn-light mx-2`}
                    onClick={()=>this.toggleMarketing()}>
                    {`Show ${this.state.showMarketing? "All": "Accept Marketing"}`}
                </button>
                <div className={`d-inline-block btn btn-light`}>
                    <select className={`btn`} onChange={(ev)=>this.sort(ev)}>
                        <option value={`id`}>Default</option>
                        <option value={`visits_high`}>Visits, high to low</option>
                        <option value={`visits_low`}>Visits, low to high</option>
                        <option value={`spend_high`}>Spend, high to low</option>
                        <option value={`spend_low`}>Spend, low to high</option>
                    </select>
                </div>
            </div>
            
            <div className={`d-flex w-100 text-uppercase font-weight-bold py-2 bg-light`}>
                <div className={`w-15`}>Name</div>
                <div className={`w-20`}>Email</div>
                <div className={`w-15`}>City</div>
                
                <div className={`w-10`}>Visits</div>
                <div className={`w-10`}>Spends</div>
                <div className={`w-10`}>Marketing</div>
                <div className={`w-20`}>Tags</div>
            </div>
            {this.state.guests.map((guest, i) => {
                return <GuestItem item={guest} key={i}/>
            })}
        </div>
    }

    toggleMarketing(){
        const marketingGuests = this.props.guests.filter((guest) => guest.allow_marketing);
        if (this.state.showMarketing){
            this.setState({
                guests: this.props.guests,
                showMarketing: false
            });
        } else {
            this.setState({
                guests: marketingGuests,
                showMarketing: true
            });
        }
    }

    sort(ev) {
        let sorted;
        switch(ev.target.value){
            case 'visits_high':
                sorted = this.state.guests.sort((a,b) => {
                    return b.visit_count - a.visit_count;
                })
                break;
            case 'visits_low':
                sorted = this.state.guests.sort((a,b) => {
                    return a.visit_count - b.visit_count;
                })
                break;

            case 'spend_high':
                sorted = this.state.guests.sort((a,b) => {
                    return b.total_spend - a.total_spend;
                })
                break;
            case 'spend_low':
                sorted = this.state.guests.sort((a,b) => {
                    return a.total_spend - b.total_spend;
                })
                break;
            case 'id':
                sorted = this.state.guests.sort((a,b) => {
                    return a.id - b.id;
                })
                break;
        }

        this.setState({
            guests: sorted,
            sortBy: ev.target.value
        });
    }
}