import React from 'react';

export default class GuestItem extends React.PureComponent {

    render() {
        return <div className={`d-flex w-100 border-bottom py-2`}>
            <div className={`w-15`}>{`${this.props.item.first_name}  ${this.props.item.last_name}`}</div>
            <div className={`w-20`}>{this.props.item.email}</div>
            <div className={`w-15`}>{this.props.item.city}</div>
            
            <div className={`w-10`}>{this.props.item.visit_count}</div>
            <div className={`w-10`}>{this.props.item.total_spend}</div>
            <div className={`w-10`}>{`${this.props.item.allow_marketing? "Yes": "No"}`}</div>
            <div className={`w-20 d-flex flex-wrap`}>
                {this.props.item.tags && this.props.item.tags.map((tag, i) => {
                    return <div className="badge badge-info m-1" key={i}>{tag}</div>
                })}
            </div>
        </div>
    }
}